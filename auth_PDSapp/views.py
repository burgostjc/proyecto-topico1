from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import CreateUserForm, PerfilForm
from django.contrib.auth.decorators import permission_required

from django.contrib.auth.models import User

from django.contrib import messages


def loginView(request):

    template_name = 'auth_PDSapp/login.html'
    data = {}

    logout(request)
    username = password = ''
    if request.POST:
        user = authenticate(
                username=request.POST['username'],
                password=request.POST['password']
            )

        print(username)
        if user is not None:
            if user.is_active:
                login(request, user)

                return redirect('PDSapp:home')
            else:
                print("usuario o pass incorrecta")
                messages.warning(
                    request,
                    'Usuario o contraseña incorrecto!'
                )

        else:
            print("no encuentra user")
            messages.error(
                request,
                'Usuario o contraseña incorrecto'
            )
    return render(request, template_name, data)


def logoutView(request):
    logout(request)
    return redirect('auth_PDSapp:login')

@permission_required('PDSapp.add_perfil')
def registerView(request):
    template_name = 'auth_PDSapp/register.html'
    data = {}

    if request.method == 'POST':
        user_form = CreateUserForm(request.POST)
        perfil_form = PerfilForm(request.POST)
        if user_form.is_valid() and perfil_form.is_valid():
            uf = user_form.save(commit=False)
            pf = perfil_form.save(commit=False)

            pf.user = uf
            uf.save()
            pf.save()
            return redirect('PDSapp:home')

        elif User.objects.filter(username=request.POST['username']).exists():
            messages.warning(
                request,
                'Este nombre usuario ya está en uso'
            )
        else:
            messages.warning(
                request,
                'Registro no válido'
            )
    else:
        user_form = CreateUserForm()
        perfil_form = PerfilForm()

    data = {
        'user_form': user_form,
        'perfil_form': perfil_form

    }

    return render(request, template_name, data)
