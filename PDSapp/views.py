from django.shortcuts import render, redirect
from PDSapp.models import Sucursal, Perfil, Dominio, Control, AspectoLegal, Recomendacion, Respuesta, Opcion
from django.contrib.auth.models import User
from PDSapp.forms import DominioForm, ControlForm, RespuestaForm, ResponderForm, AspectoLegalForm, RecomendacionForm, SucursalForm
from django.contrib.auth.decorators import login_required, permission_required
from tablib import Dataset
from .resources import PerfilResource
from django.db.models import Q
from django.http import HttpResponse
from PDSapp.utils import render_to_pdf
from django.template.loader import get_template


@login_required
def home(request):
    template_name = 'PDSapp/home.html'
    data = {}

    data['query'] = ''

    return render(request, template_name, data)

#arreglar
def excel_usuarios(request):
    if request.method == 'POST':
        perfil_resource = PerfilResource()
        dataset = Dataset()
        new_persons = request.FILES['myfile']

        imported_data = dataset.load(new_persons.read())
        result = perfil_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            perfil_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'PDSapp/excel_usuarios.html')


@login_required
def lista_dominio(request):
	data = {}
	template_name = 'PDSapp/lista_dominio.html'
	data['dominios'] = Dominio.objects.all()

	return render(request, template_name, data)

@permission_required('PDSapp.add_dominio')
def crear_dominio(request):
	data = {}
	template_name = 'PDSapp/crear_dominio.html'

	if request.method == 'POST':
		data['form'] = DominioForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_dominio')
	else:
		data['form'] = DominioForm()

	return render(request, template_name, data)

@permission_required('PDSapp.change_dominio')
def editar_dominio(request, dominio_id):
	data = {}
	template_name = 'PDSapp/editar_dominio.html'
	dominio = Dominio.objects.get(pk=dominio_id)

	if request.method == 'POST':
		data['form'] = DominioForm(request.POST,request.FILES or None, instance=dominio)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_dominio')
	else:
		data['form'] = DominioForm(instance=dominio)

	return render(request, template_name, data)

@permission_required('PDSapp.delete_dominio')
def eliminar_dominio(request, dominio_id):
	data = {}
	template_name = 'PDSapp/eliminar_dominio.html'
	data['dominio'] = Dominio.objects.get(pk=dominio_id)

	if request.method == 'POST':
		data['dominio'].delete()
		return redirect('PDSapp:lista_dominio')

	return render(request, template_name, data)


@login_required
def lista_control(request):
	data = {}
	template_name = 'PDSapp/lista_control.html'
	data['controles'] = Control.objects.all()

	return render(request, template_name, data)

@permission_required('PDSapp.add_control')
def crear_control(request):
	data = {}
	template_name = 'PDSapp/crear_control.html'

	if request.method == 'POST':
		data['form'] = ControlForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_control')
	else:
		data['form'] = ControlForm()

	return render(request, template_name, data)

@permission_required('PDSapp.change_control')
def editar_control(request, control_id):
	data = {}
	template_name = 'PDSapp/editar_control.html'
	control = Control.objects.get(pk=control_id)

	if request.method == 'POST':
		data['form'] = ControlForm(request.POST,request.FILES or None, instance=control)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_control')
	else:
		data['form'] = ControlForm(instance=control)

	return render(request, template_name, data)

@permission_required('PDSapp.delete_control')
def eliminar_control(request, control_id):
	data = {}
	template_name = 'PDSapp/eliminar_control.html'
	data['control'] = Control.objects.get(pk=control_id)

	if request.method == 'POST':
		data['control'].delete()
		return redirect('PDSapp:lista_control')

	return render(request, template_name, data)


@permission_required('PDSapp.view_respuesta')
def lista_respuesta(request):
	data = {}
	template_name = 'PDSapp/lista_respuesta.html'
	data['respuestas'] = Respuesta.objects.all()

	return render(request, template_name, data)

@permission_required('PDSapp.add_respuesta')
def crear_respuesta(request):
	data = {}
	template_name = 'PDSapp/crear_respuesta.html'

	if request.method == 'POST':
		data['form'] = RespuestaForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_respuesta')
	else:
		data['form'] = RespuestaForm()

	return render(request, template_name, data)

@permission_required('PDSapp.change_respuesta')
def editar_respuesta(request, respuesta_id):
	data = {}
	template_name = 'PDSapp/editar_respuesta.html'
	respuesta = Respuesta.objects.get(pk=respuesta_id)

	if request.method == 'POST':
		data['form'] = RespuestaForm(request.POST,request.FILES or None, instance=respuesta)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_respuesta')
	else:
		data['form'] = RespuestaForm(instance=respuesta)

	return render(request, template_name, data)

@permission_required('PDSapp.delete_respuesta')
def eliminar_respuesta(request, respuesta_id):
	data = {}
	template_name = 'PDSapp/eliminar_respuesta.html'
	data['respuestas'] = Respuesta.objects.get(pk=respuesta_id)

	if request.method == 'POST':
		data['respuestas'].delete()
		return redirect('PDSapp:lista_respuesta')

	return render(request, template_name, data)


@login_required
def lista_responder(request):
	data = {}
	template_name = 'PDSapp/lista_responder.html'
	data['opciones'] = Opcion.objects.filter(respuesta__perfil__user=request.user)

	return render(request, template_name, data)

@login_required
def crear_responder(request):
	data = {}
	template_name = 'PDSapp/crear_responder.html'

	if request.method == 'POST':
		data['form'] = ResponderForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_responder')
	else:
		data['form'] = ResponderForm()

	return render(request, template_name, data)

@login_required
def editar_responder(request, opcion_id):
	data = {}
	template_name = 'PDSapp/editar_responder.html'
	opcion = Opcion.objects.get(pk=opcion_id)

	if request.method == 'POST':
		data['form'] = ResponderForm(request.POST,request.FILES or None, instance=opcion)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_responder')
	else:
		data['form'] = ResponderForm(instance=opcion)

	return render(request, template_name, data)

@login_required
def eliminar_responder(request, opcion_id):
	data = {}
	template_name = 'PDSapp/eliminar_responder.html'
	data['opciones'] = Opcion.objects.get(pk=opcion_id)

	if request.method == 'POST':
		data['opciones'].delete()
		return redirect('PDSapp:lista_responder')

	return render(request, template_name, data)


@login_required
def lista_aspecto_legal(request):
	data = {}
	template_name = 'PDSapp/lista_aspecto_legal.html'
	data['aspectoslegales'] = AspectoLegal.objects.all()

	return render(request, template_name, data)

@permission_required('PDSapp.add_aspectolegal')
def crear_aspecto_legal(request):
	data = {}
	template_name = 'PDSapp/crear_aspecto_legal.html'

	if request.method == 'POST':
		data['form'] = AspectoLegalForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_aspecto_legal')
	else:
		data['form'] = AspectoLegalForm()

	return render(request, template_name, data)

@permission_required('PDSapp.change_aspectolegal')
def editar_aspecto_legal(request, aspectolegal_id):
	data = {}
	template_name = 'PDSapp/editar_aspecto_legal.html'
	aspectoslegales = AspectoLegal.objects.get(pk=aspectolegal_id)

	if request.method == 'POST':
		data['form'] = AspectoLegalForm(request.POST,request.FILES or None, instance=aspectoslegales)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_aspecto_legal')
	else:
		data['form'] = AspectoLegalForm(instance=aspectoslegales)

	return render(request, template_name, data)

@permission_required('PDSapp.delete_aspectolegal')
def eliminar_aspecto_legal(request, aspectolegal_id):
	data = {}
	template_name = 'PDSapp/eliminar_aspecto_legal.html'
	data['aspectoslegales'] = AspectoLegal.objects.get(pk=aspectolegal_id)

	if request.method == 'POST':
		data['aspectoslegales'].delete()
		return redirect('PDSapp:lista_aspecto_legal')

	return render(request, template_name, data)


@login_required
def lista_recomendacion(request):
	data = {}
	template_name = 'PDSapp/lista_recomendacion.html'
	data['recomendaciones'] = Recomendacion.objects.all()

	return render(request, template_name, data)

@permission_required('PDSapp.add_recomendacion')
def crear_recomendacion(request):
	data = {}
	template_name = 'PDSapp/crear_recomendacion.html'

	if request.method == 'POST':
		data['form'] = RecomendacionForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_recomendacion')
	else:
		data['form'] = RecomendacionForm()

	return render(request, template_name, data)

@permission_required('PDSapp.change_recomendacion')
def editar_recomendacion(request, recomendacion_id):
	data = {}
	template_name = 'PDSapp/editar_recomendacion.html'
	recomendaciones = Recomendacion.objects.get(pk=recomendacion_id)

	if request.method == 'POST':
		data['form'] = RecomendacionForm(request.POST,request.FILES or None, instance=recomendaciones)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_recomendacion')
	else:
		data['form'] = RecomendacionForm(instance=recomendaciones)

	return render(request, template_name, data)

@permission_required('PDSapp.delete_recomendacion')
def eliminar_recomendacion(request, recomendacion_id):
	data = {}
	template_name = 'PDSapp/eliminar_recomendacion.html'
	data['recomendaciones'] = Recomendacion.objects.get(pk=recomendacion_id)

	if request.method == 'POST':
		data['recomendaciones'].delete()
		return redirect('PDSapp:lista_recomendacion')

	return render(request, template_name, data)


@login_required
def lista_sucursal(request):
	data = {}
	template_name = 'PDSapp/lista_sucursal.html'
	data['sucursales'] = Sucursal.objects.all()

	return render(request, template_name, data)

@permission_required('PDSapp.add_sucursal')
def crear_sucursal(request):
	data = {}
	template_name = 'PDSapp/crear_sucursal.html'

	if request.method == 'POST':
		data['form'] = SucursalForm(request.POST,files=request.FILES)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_sucursal')
	else:
		data['form'] = SucursalForm()

	return render(request, template_name, data)

@permission_required('PDSapp.change_sucursal')
def editar_sucursal(request, sucursal_id):
	data = {}
	template_name = 'PDSapp/editar_sucursal.html'
	sucursal = Sucursal.objects.get(pk=sucursal_id)

	if request.method == 'POST':
		data['form'] = SucursalForm(request.POST,request.FILES or None, instance=sucursal)
		if data['form'].is_valid():
			data['form'].save()
			return redirect('PDSapp:lista_sucursal')
	else:
		data['form'] = SucursalForm(instance=sucursal)

	return render(request, template_name, data)

@permission_required('PDSapp.delete_sucursal')
def eliminar_sucursal(request, sucursal_id):
	data = {}
	template_name = 'PDSapp/eliminar_sucursal.html'
	data['sucursal'] = Sucursal.objects.get(pk=sucursal_id)

	if request.method == 'POST':
		data['sucursal'].delete()
		return redirect('PDSapp:lista_sucursal')

	return render(request, template_name, data)


@login_required
def informe_diagnostico(request):
	data = {}
	template_name = 'PDSapp/informe_diagnostico.html'
	data['controles'] = Control.objects.all()

	return render(request, template_name, data)

@login_required
def informe_diagnostico_pdf(request, *args, **kwargs):
	template = get_template('PDSapp/informe_diagnostico_pdf.html')
	context = {
		"controles": Control.objects.all(),
	}
	html = template.render(context)
	pdf = render_to_pdf('PDSapp/informe_diagnostico_pdf.html', context)
	if pdf:
		response = HttpResponse(pdf, content_type='application/pdf')
		filename = "Informe Diagnositco"
		content = "inline; filename='%s'" %(filename)
		download = request.GET.get("download")
		if download:
			content = "attachment; filename='%s'" %(filename)
		response['Content-Disposition'] = content
		return response
	return HttpResponse("Not found")

@login_required
def informe_pds(request):
	data = {}
	template_name = 'PDSapp/informe_pds.html'
	data['controles'] = Control.objects.all()

	return render(request, template_name, data)

@login_required
def informe_pds_pdf(request, *args, **kwargs):
	template = get_template('PDSapp/informe_pds_pdf.html')
	context = {
		"controles": Control.objects.all(),
	}
	html = template.render(context)
	pdf = render_to_pdf('PDSapp/informe_pds_pdf.html', context)
	if pdf:
		response = HttpResponse(pdf, content_type='application/pdf')
		filename = "Informe Plan Director de Seguridad"
		content = "inline; filename='%s'" %(filename)
		download = request.GET.get("download")
		if download:
			content = "attachment; filename='%s'" %(filename)
		response['Content-Disposition'] = content
		return response
	return HttpResponse("Not found")