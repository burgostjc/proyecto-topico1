from django import forms
from .models import Perfil, Dominio, Control, Respuesta, AspectoLegal, Recomendacion, Opcion, Sucursal
from django.contrib.auth.models import User


class PerfilForm(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = '__all__'

class DominioForm(forms.ModelForm):
    class Meta:
        model = Dominio
        fields = '__all__'

class ControlForm(forms.ModelForm):
    class Meta:
        model = Control
        fields = '__all__'

class RespuestaForm(forms.ModelForm):
    class Meta:
        model = Respuesta
        fields = '__all__'

class ResponderForm(forms.ModelForm):
    class Meta:
        model = Opcion
        fields = ['dato']

class AspectoLegalForm(forms.ModelForm):
    class Meta:
        model = AspectoLegal
        fields = '__all__'

class RecomendacionForm(forms.ModelForm):
    class Meta:
        model = Recomendacion
        fields = '__all__'

class SucursalForm(forms.ModelForm):
    class Meta:
        model = Sucursal
        fields = '__all__'