from django.db import models
from django.contrib.auth.models import User
from django.db.models import Q

class Sucursal(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre


rol = (
    ('OPERATIVO', 'Operativo'),
    ('DIRECTIVO', 'Directivo'),
    ('AMBOS', 'Ambos'),
)

class Perfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    apellido_materno = models.CharField(max_length=50)
    sucursal = models.ForeignKey(Sucursal, on_delete=models.CASCADE)
    cargo = models.CharField(choices=rol, max_length=9)

    def __str__(self):
        return str(self.user)

class Dominio(models.Model):
    perfil = models.ManyToManyField(Perfil)
    nombre = models.CharField(max_length=50)

    class Meta:
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

class Control(models.Model):
    dominio = models.ForeignKey(Dominio, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=50)

    def get_promedio(self):
        numr = Respuesta.objects.filter(control=self.control).count()
        sumar = Respuesta.objects.get(get_percentage)
        perc = sumar / numr
        return perc

    class Meta:
        ordering = ['dominio']

    def __str__(self):
        return self.nombre


class Respuesta(models.Model):
    perfil = models.ForeignKey(Perfil, on_delete=models.CASCADE)
    control = models.ForeignKey(Control, on_delete=models.CASCADE)

    def get_percentage(self):
        total_count = Opcion.objects.filter((Q(respuesta__control=self.control) & (Q(dato='SI'))) | (Q(respuesta__control=self.control) & (Q(dato='NO'))) | (Q(respuesta__control=self.control) & (Q(dato='DESCONOZCO')))).count()
        cnt = Opcion.objects.filter(Q(respuesta__control=self.control) & (Q(dato='SI'))).count()
        perc = cnt * 100 / total_count
        return perc

    class Meta:
        ordering = ['control']

    def __str__(self):
        return str(self.control)


opc = (
    ('SI', 'Si'),
    ('NO', 'No'),
    ('DESCONOZCO', 'Desconozco'),
)
class Opcion(models.Model):
    respuesta = models.OneToOneField(Respuesta, on_delete=models.CASCADE)
    dato = models.CharField(choices=opc, max_length=10)

    def __str__(self):
        return self.dato

class AspectoLegal(models.Model):
    dominio = models.ManyToManyField(Dominio)
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField()

    def __str__(self):
        return self.nombre

class Recomendacion(models.Model):
    control = models.ForeignKey(Control, on_delete=models.CASCADE)
    descripcion = models.TextField()
    fuente = models.CharField(max_length=50)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField()
    fecha_aplicacion = models.DateTimeField()
    aprobado = models.CharField(max_length=50)
    estado = models.CharField(max_length=50)


    def __str__(self):
        return self.dominio