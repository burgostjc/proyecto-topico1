from django.contrib import admin

from PDSapp.models import (
    Sucursal,
    Perfil,
    Dominio,
    Control,
    AspectoLegal,
    Recomendacion,
    Respuesta,
    Opcion
)


@admin.register(Sucursal)
class SucursalAdmin(admin.ModelAdmin):
	pass


@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
	pass

@admin.register(Dominio)
class DominioAdmin(admin.ModelAdmin):
    pass

@admin.register(Control)
class ControlAdmin(admin.ModelAdmin):
    pass

@admin.register(AspectoLegal)
class AspectoLegalAdmin(admin.ModelAdmin):
    pass

@admin.register(Recomendacion)
class RecomendacionAdmin(admin.ModelAdmin):
    pass

@admin.register(Respuesta)
class RespuestaAdmin(admin.ModelAdmin):
    pass

@admin.register(Opcion)
class OpcionAdmin(admin.ModelAdmin):
    pass