from django.urls import path
from PDSapp import views

app_name = 'PDSapp'

urlpatterns = [
	path('', views.home , name='home'),
	path('excel_de_usuarios', views.excel_usuarios , name='excel_usuarios'),

	path('lista_dominio', views.lista_dominio , name='lista_dominio'),
	path('crear_dominio', views.crear_dominio , name='crear_dominio'),
	path('editar_dominio/<int:dominio_id>', views.editar_dominio , name='editar_dominio'),
	path('eliminar_dominio/<int:dominio_id>', views.eliminar_dominio, name = 'eliminar_dominio'),

	path('lista_control', views.lista_control , name='lista_control'),
	path('crear_control', views.crear_control , name='crear_control'),
	path('editar_control/<int:control_id>', views.editar_control , name='editar_control'),
	path('eliminar_control/<int:control_id>', views.eliminar_control, name = 'eliminar_control'),

	path('lista_respuesta', views.lista_respuesta , name='lista_respuesta'),
	path('crear_respuesta', views.crear_respuesta , name='crear_respuesta'),
	path('editar_respuesta/<int:respuesta_id>', views.editar_respuesta , name='editar_respuesta'),
	path('eliminar_respuesta/<int:respuesta_id>', views.eliminar_respuesta, name = 'eliminar_respuesta'),

	path('lista_responder', views.lista_responder , name='lista_responder'),
	path('crear_responder', views.crear_responder , name='crear_responder'),
	path('editar_responder/<int:opcion_id>', views.editar_responder , name='editar_responder'),
	path('eliminar_responder/<int:opcion_id>', views.eliminar_responder, name = 'eliminar_responder'),

	path('lista_aspecto_legal', views.lista_aspecto_legal , name='lista_aspecto_legal'),
	path('crear_aspecto_legal', views.crear_aspecto_legal , name='crear_aspecto_legal'),
	path('editar_aspecto_legal/<int:aspectolegal_id>', views.editar_aspecto_legal , name='editar_aspecto_legal'),
	path('eliminar_aspecto_legal/<int:aspectolegal_id>', views.eliminar_aspecto_legal, name = 'eliminar_aspecto_legal'),

	path('lista_recomendacion', views.lista_recomendacion , name='lista_recomendacion'),
	path('crear_recomendacion', views.crear_recomendacion , name='crear_recomendacion'),
	path('editar_recomendacion/<int:recomendacion_id>', views.editar_recomendacion , name='editar_recomendacion'),
	path('eliminar_recomendacion/<int:recomendacion_id>', views.eliminar_recomendacion, name = 'eliminar_recomendacion'),

	path('lista_sucursal', views.lista_sucursal , name='lista_sucursal'),
	path('crear_sucursal', views.crear_sucursal , name='crear_sucursal'),
	path('editar_sucursal/<int:sucursal_id>', views.editar_sucursal , name='editar_sucursal'),
	path('eliminar_sucursal/<int:sucursal_id>', views.eliminar_sucursal, name = 'eliminar_sucursal'),

	path('informe_diagnostico', views.informe_diagnostico , name='informe_diagnostico'),
	path('informe_diagnostico_pdf', views.informe_diagnostico_pdf , name='informe_diagnostico_pdf'),
	path('informe_pds', views.informe_pds , name='informe_pds'),
	path('informe_pds_pdf', views.informe_pds_pdf , name='informe_pds_pdf'),
]
