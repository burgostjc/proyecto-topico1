from django.apps import AppConfig


class PdsappConfig(AppConfig):
    name = 'PDSapp'
