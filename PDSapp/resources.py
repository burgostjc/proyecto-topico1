from import_export import resources
from .models import Perfil
from django.contrib.auth.models import User

class CreateUserResource(resources.ModelResource):
    class Meta:
        model = User
        fields = ['username',
            'first_name',
            'last_name',
            'email',
            'password',
        ]

class PerfilResource(resources.ModelResource):
    class Meta:
        model = Perfil
        fields = ['apellido_materno',
        	'sucursal',
        	'cargo'
        ]